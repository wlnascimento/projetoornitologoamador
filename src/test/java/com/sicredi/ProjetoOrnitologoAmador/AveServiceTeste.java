package com.sicredi.ProjetoOrnitologoAmador;

import com.sicredi.ProjetoOrnitologoAmador.model.Ave;
import com.sicredi.ProjetoOrnitologoAmador.model.Usuario;
import com.sicredi.ProjetoOrnitologoAmador.repository.AveRepository;
import com.sicredi.ProjetoOrnitologoAmador.service.AveService;
import com.sicredi.ProjetoOrnitologoAmador.util.CorPredominante;
import com.sicredi.ProjetoOrnitologoAmador.util.Genero;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertThrows;

@WebMvcTest(AveService.class)
public class AveServiceTeste {

    @MockBean
    private AveRepository aveRepository;

    @MockBean
    private AveService aveService;

    private Ave novaAve;
    private List<Ave> avesList;
    private List<Ave> avesListEmpty;

    @BeforeEach
    public void setUp(){
        aveRepository = Mockito.mock(AveRepository.class);
        aveService = new AveService(aveRepository);
        avesList = asList(new Ave(null,"Papagaio",20, Genero.valueOf("Macho"), CorPredominante.valueOf("VerdeAmarelado") ,"Psittacidae",null));
        avesListEmpty = new ArrayList<>();
    }

    @Test
    void buscarAvePorNome_isEmpty() throws Exception {

        Mockito.when(aveRepository.findAll()).thenReturn(avesListEmpty);

        ResponseEntity<List<Ave>> avesList = aveService.buscarAvePorNome("Teste");

        Assertions.assertEquals(avesList.getStatusCode(), HttpStatus.NO_CONTENT);
        System.out.println("Status: " + avesList.getStatusCode());
    }

    @Test
    void buscarAvePorNome_RetornoOK() throws Exception {

        Mockito.when(aveRepository.findAll()).thenReturn(avesList);

        ResponseEntity<List<Ave>> avesList = aveService.buscarAvePorNome("Papagaio");

        Assertions.assertEquals(avesList.getStatusCode(), HttpStatus.FOUND);
        System.out.println("Status: " + avesList.getStatusCode());
    }


    @Test
    void salvarAve_RetornoOK() throws Exception {
        novaAve = new Ave(null,"Canario",20, Genero.valueOf("Femea"), CorPredominante.valueOf("Amarelo") ,"Psittacidae",null);
        Mockito.when(aveRepository.findAll()).thenReturn(avesList);

        ResponseEntity<Ave> ave = aveService.salvarAve(novaAve);

        Assertions.assertEquals(ave.getStatusCode(), HttpStatus.CREATED);
        System.out.println("Status: " + ave.getStatusCode());
    }


    @Test
    void salvarAve_AveJaCadastrada() throws Exception {
        novaAve = new Ave(null,"Papagaio",20, Genero.valueOf("Macho"), CorPredominante.valueOf("VerdeAmarelado") ,"Psittacidae",null);
        Mockito.when(aveRepository.findAll()).thenReturn(avesList);

        RuntimeException excp = assertThrows(RuntimeException.class, () -> aveService.salvarAve(novaAve));
        System.out.println("\n\nException: " + excp.toString());
    }

}

