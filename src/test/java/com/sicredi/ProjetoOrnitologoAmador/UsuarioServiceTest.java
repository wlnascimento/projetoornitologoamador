package com.sicredi.ProjetoOrnitologoAmador;

import com.sicredi.ProjetoOrnitologoAmador.model.Usuario;
import com.sicredi.ProjetoOrnitologoAmador.repository.UsuarioRepository;
import com.sicredi.ProjetoOrnitologoAmador.service.UsuarioService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertThrows;

@WebMvcTest(UsuarioService.class)
public class UsuarioServiceTest {

    @MockBean
    private UsuarioRepository usuarioRepository;

    @MockBean
    private UsuarioService usuarioService;

    @MockBean
    private PasswordEncoder passwordEncoder;

    private Usuario novoUsuario;
    private List<Usuario> usuarioList;
    private List<Usuario> usuarioListEmpty;

    @BeforeEach
    public void setUp(){
        usuarioRepository = Mockito.mock(UsuarioRepository.class);
        usuarioService = new UsuarioService(usuarioRepository,passwordEncoder);
        usuarioList = asList(new Usuario(1L,"Wiliam","Wiliam","123"), new Usuario(2L,"Pedro","Pedro","1234"));
        usuarioListEmpty = new ArrayList<>();
    }

    //@DisplayName("Test Spring @Autowired Integration")
    @Test
    void cadastrarUsuario_CadastroOK_Retorno201() throws Exception {

        novoUsuario = new Usuario(1L,"Wiliam","Wiliam","123");
        Mockito.when(usuarioRepository.findAll()).thenReturn(usuarioListEmpty);

        ResponseEntity<Usuario> usuario = usuarioService.salvarUsuario(novoUsuario);

        Assertions.assertEquals(usuario.getStatusCode(),HttpStatus.CREATED);
        System.out.println("Status: " + usuario.getStatusCode());
    }


    @Test
    void cadastrarUsuario_IdJaCadastrado() throws Exception {
        novoUsuario = new Usuario(1L,"Wiliam","Wiliam","123");

        Mockito.when(usuarioRepository.findAll()).thenReturn(usuarioList);
        Mockito.when(usuarioRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(novoUsuario));

        RuntimeException excp = assertThrows(RuntimeException.class, () -> usuarioService.salvarUsuario(novoUsuario));
        System.out.println("\n\nException: " + excp.toString());
    }


    @Test
    void cadastrarUsuario_UserIdJaCadastrado() throws Exception {
        novoUsuario = new Usuario(null,"Wiliam","Wiliam","123");
        Mockito.when(usuarioRepository.findAll()).thenReturn(usuarioList);
        Mockito.when(usuarioRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(novoUsuario));

        RuntimeException excp = assertThrows(RuntimeException.class, () -> usuarioService.salvarUsuario(novoUsuario));
        System.out.println("\n\nException: " + excp.toString());
    }

    @Test
    void consultarUsuario_UsuarioEncontrado() throws Exception{
        Mockito.when(usuarioRepository.findAll()).thenReturn(usuarioList);

        ResponseEntity<Usuario> usuario = usuarioService.consultarUsuario("Wiliam");
        Assertions.assertEquals(usuario.getStatusCode(),HttpStatus.FOUND);
        System.out.println("Status: " + usuario.getStatusCode());
        System.out.println("Usuario: " + usuario.getBody().toString());
    }

    @Test
    void consultarUsuario_UsuarioNaoEncontrado() throws Exception{
        Mockito.when(usuarioRepository.findAll()).thenReturn(usuarioListEmpty);

        RuntimeException excp = assertThrows(RuntimeException.class, () -> usuarioService.consultarUsuario("Wiliam"));
        System.out.println("\n\nException: " + excp.toString());
    }

}
