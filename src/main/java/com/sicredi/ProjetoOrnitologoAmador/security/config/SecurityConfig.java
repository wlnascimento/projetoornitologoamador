package com.sicredi.ProjetoOrnitologoAmador.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true) // validação pelos motodos REST
public class SecurityConfig{

    @Autowired
    UserDetailsServiceImpl userDetailsService;

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .httpBasic()
                .and()
                .authorizeHttpRequests()
                .antMatchers("/").permitAll()
                .antMatchers(HttpMethod.POST, "/Usuarios/**").permitAll()
                .antMatchers(HttpMethod.GET, "/Aves/**").permitAll()
                .antMatchers(HttpMethod.GET, "/Ocorrencias/**").hasAuthority("USER")
                .antMatchers(HttpMethod.POST, "/Aves/**").hasAuthority("USER")
                .antMatchers(HttpMethod.POST, "/Ocorrencias/**").hasAuthority("USER")
                .antMatchers(HttpMethod.PUT, "/Aves/**").hasAuthority("USER")
                .antMatchers(HttpMethod.PUT, "/Ocorrencias/**").hasAuthority("USER")
                .antMatchers(HttpMethod.DELETE, "/Aves/**").hasAuthority("USER")
                .antMatchers(HttpMethod.DELETE, "/Ocorrencias/**").hasAuthority("USER")
                .anyRequest().authenticated()
                .and()
                .csrf().disable();
        return http.build();
    }

    protected void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
