package com.sicredi.ProjetoOrnitologoAmador.kafka;

import com.sicredi.ProjetoOrnitologoAmador.model.Ave;
import com.sicredi.ProjetoOrnitologoAmador.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
/*
@RestController
@RequestMapping("/Kafka")
public class KafkaProducer {

    @Autowired
    private KafkaTemplate<String, Ave> kafkaTemplate;

    @Autowired
    UsuarioRepository usuarioRepository;

    @PostMapping("/cadastrarAve")
    public void producer(@RequestBody Ave ave){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String name;

        if (principal instanceof UserDetails) {
            name = ((UserDetails)principal).getUsername();
        } else {
            name = principal.toString();
        }


        Message<Ave> msg = MessageBuilder.withPayload(ave).
                setHeader(KafkaHeaders.TOPIC, "topicoAve").
                build();

        kafkaTemplate.send(msg);
    }
}
*/