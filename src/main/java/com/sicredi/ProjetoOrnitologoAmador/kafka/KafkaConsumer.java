package com.sicredi.ProjetoOrnitologoAmador.kafka;

import com.sicredi.ProjetoOrnitologoAmador.model.Ave;
import com.sicredi.ProjetoOrnitologoAmador.service.AveService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
/*
@AllArgsConstructor
@Service
public class KafkaConsumer {
    @Autowired
    AveService aveService;

    @KafkaListener(topics="topicoAve", groupId = "teste-groupId" )
    public void consumer(Ave ave) throws Exception {
        aveService.salvarAve(ave);
    }

}
 */
