package com.sicredi.ProjetoOrnitologoAmador.model;

import com.sicredi.ProjetoOrnitologoAmador.util.CorPredominante;
import com.sicredi.ProjetoOrnitologoAmador.util.Genero;
import com.sicredi.ProjetoOrnitologoAmador.util.Habitat;

import lombok.*;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "Ave")
public class Ave {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_AVE")
    @Getter private Long id;
    @Getter @Setter private String nome;
    @Getter @Setter private String nomeIngles;
    @Getter @Setter private String nomeLatim;
    @Getter @Setter private Integer tamanho;

    @Enumerated(EnumType.STRING)
    @Getter @Setter private Genero genero;

    @Enumerated(EnumType.STRING)
    @Getter @Setter private CorPredominante corPredominante;

    @Getter @Setter private String familia;

    @ElementCollection(targetClass = Habitat.class, fetch = FetchType.EAGER)
    //@JoinTable(name = "tb_avehabitats", joinColumns = @JoinColumn(name = "avehabitats_id"))
    //@Column(name ="avehabitats")
    //@Enumerated(EnumType.STRING)
    @Convert(converter = Habitat.Mapeador.class)
    @Getter private List<Habitat> habitats = new ArrayList<>();

    public Ave(Long id, String nome, Integer tamanho, Genero genero, CorPredominante corPredominante, String familia, List<Habitat> habitats) {
        this.id = id;
        this.nome = nome;
        this.tamanho = tamanho;
        this.genero = genero;
        this.corPredominante = corPredominante;
        this.familia = familia;
        this.habitats = habitats;
    }

    public void adicionaHabitat(Habitat novoHabitat){
        this.habitats.add(novoHabitat);
    }

    public void removeHabitat(Habitat novoHabitat){

        this.habitats.remove(novoHabitat);
    }
}
