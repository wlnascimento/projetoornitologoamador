package com.sicredi.ProjetoOrnitologoAmador.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import javax.persistence.*;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "Ocorrencia")
public class Ocorrencia {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_OCORRENCIA")
    @Getter private Long id;

    @OneToOne
    @Getter @Setter private Ave ave;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Getter @Setter private LocalDateTime dataHoraAvistamento;

    @Getter @Setter private String localAvistamento;
    @Getter @Setter private Integer[] dadosDoGuia = new Integer[3];


    public Ocorrencia(Long id, Ave ave, String localAvistamento) {
        this.id = id;
        this.ave = ave;
        this.dataHoraAvistamento = LocalDateTime.now();
        this.localAvistamento = localAvistamento;
    }
}
