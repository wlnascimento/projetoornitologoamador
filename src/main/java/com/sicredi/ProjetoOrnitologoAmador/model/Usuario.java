package com.sicredi.ProjetoOrnitologoAmador.model;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;


@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "Usuario")
public class Usuario {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_USUARIO", unique=true)
    @Getter private Long id;

    @NotBlank(message = "Nome é obrigatório")
    @Length(min = 4, max = 60, message = "Nome deve possuir entre entre 4 e 60 caracteres")
    @Getter @Setter private String nome;

    @NotBlank(message = "Nome de Usuário é obrigatório ")
    @Length(min = 4, max = 15, message = "Nome de usuário deve possuir entre 4 e 15 caracteres")
    @Column(unique = true, nullable = false)
    @Getter @Setter private String userID;

    @OneToMany(cascade = CascadeType.ALL)
    @Getter private List<Ocorrencia> ocorrencias = new ArrayList<>();

    @Column(nullable = false)
    @Getter @Setter private String senha;

    @Getter @Setter private String role;


    public Usuario(Long id, String nome, String userID, String senha) {
        this.id = id;
        this.nome = nome;
        this.userID = userID;
        this.senha = senha;
    }

    public Usuario(String nome, String username, String password) {
        this.nome = nome;
        this.userID = username;
        this.senha = password;
    }


    public void adicionaOcorrencia(Ocorrencia novaOcorrencia){
        this.ocorrencias.add(novaOcorrencia);
    }

    public void removeOcorrencia(Ocorrencia novaOcorrencia){
        this.ocorrencias.remove(novaOcorrencia);
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", userID='" + userID + '\'' +
                ", senha='" + senha + '\'' +
                ", ocorrencias=" + ocorrencias +
                '}';
    }
}
