package com.sicredi.ProjetoOrnitologoAmador;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetoOrnitologoAmadorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetoOrnitologoAmadorApplication.class, args);
	}

}
