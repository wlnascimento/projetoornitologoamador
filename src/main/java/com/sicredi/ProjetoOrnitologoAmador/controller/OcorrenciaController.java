package com.sicredi.ProjetoOrnitologoAmador.controller;

import com.sicredi.ProjetoOrnitologoAmador.model.Ocorrencia;
import com.sicredi.ProjetoOrnitologoAmador.service.OcorrenciaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 200 - SUCCESS
 * 404 - RESOURCE NOT FOUND
 * 400 - BAD REQUEST
 * 201 - CREATED
 * 401 - UNAUTHORIZED
 * 415 - UNSUPPORTED TYPE
 * 500 - SERVER ERROR
 */
@RestController
@RequestMapping("/Ocorrencias")
public class OcorrenciaController {

    @Autowired OcorrenciaService ocorrenciaService;

    @PostMapping("/CadastrarOcorrencia/{nomeUsuario}")
    public ResponseEntity<Ocorrencia> cadastrarOcorrencia(@Valid @PathVariable String nomeUsuario, @RequestBody Ocorrencia novaOcorrencia){
        return ocorrenciaService.cadastrarOcorrenciaUsuario(nomeUsuario,novaOcorrencia);
    }

}
