package com.sicredi.ProjetoOrnitologoAmador.controller;

import com.sicredi.ProjetoOrnitologoAmador.model.Ave;
import com.sicredi.ProjetoOrnitologoAmador.service.AveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RequestMapping("/Aves")
@RestController
public class AveController {

    @Autowired AveService aveService;

    @GetMapping("/ListarTodasAves")
    public List<Ave> listAll(){
        return aveService.listAll();
    }

    @GetMapping("/BuscarAvePorNome/{nome}")
    public ResponseEntity<List<Ave>> buscarAvePorNome(@PathVariable String nome){
        return aveService.buscarAvePorNome(nome);
    }

    @PostMapping("/CadastrarAve")
    public ResponseEntity<Ave> cadastrarAve(@RequestBody Ave novaAve){
        return aveService.salvarAve(novaAve);
    }

}
