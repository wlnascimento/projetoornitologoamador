package com.sicredi.ProjetoOrnitologoAmador.controller;

import com.sicredi.ProjetoOrnitologoAmador.model.Ocorrencia;
import com.sicredi.ProjetoOrnitologoAmador.model.Usuario;
import com.sicredi.ProjetoOrnitologoAmador.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


/**
 * 200 - SUCCESS
 * 404 - RESOURCE NOT FOUND
 * 400 - BAD REQUEST
 * 201 - CREATED
 * 401 - UNAUTHORIZED
 * 415 - UNSUPPORTED TYPE
 * 500 - SERVER ERROR
 */
@RestController
@RequestMapping("/Usuarios")
public class UsuarioController {

    @Autowired private UsuarioService usuarioService;
    @PostMapping("/CadastrarUsuario")
    public ResponseEntity<Usuario> cadastrarUsuario(@Valid @RequestBody Usuario novoUsuario){
        return usuarioService.salvarUsuario(novoUsuario);
    }
    @GetMapping("/ConsultaUsuario/{nomeUsuario}")
    public ResponseEntity<Usuario> consultarUsuario(@Valid @PathVariable String nomeUsuario){
        return usuarioService.consultarUsuario(nomeUsuario);
    }
    @GetMapping("/ConsultaOcorrenciasUsuario/{nomeUsuario}")
    public ResponseEntity<List<Ocorrencia>> listarOcorrenciaDoUsuario(@Valid @PathVariable String nomeUsuario){
        return usuarioService.listarOcorrenciaDoUsuario(nomeUsuario);
    }

    @PostMapping("/login")
    public ResponseEntity<Boolean> loginUsuario(@RequestBody Usuario usuario) throws Exception{
        return usuarioService.login(usuario);
    }

}
