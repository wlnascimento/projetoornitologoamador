package com.sicredi.ProjetoOrnitologoAmador.util;

public enum Genero {
    Macho,
    Femea,
    Jovem,
    NaoIdentificado
}
