package com.sicredi.ProjetoOrnitologoAmador.util;

public enum CorPredominante {
    Branco,
    Preto,
    Cinza,
    CinzaClaro,
    CinzaEscuro,
    Marrom,
    VerdeAmarelado,
    Bege,
    Amarelo
}
