package com.sicredi.ProjetoOrnitologoAmador.util;

import lombok.*;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.*;

public enum Habitat {
    CSB ("Campo Seco Baixo"),
    CAUVA("Campo alagado, Campo úmido, Várzeas alagadas"),
    BED("Banhado com Espelho d’água"),
    CSACS("Campo seco alto campo sujo"),
    CCAAA("Campo com árvores, arbustos ou arvoretas"),
    BVA("Banhado com vegetação alta, em geral palha ou gravatá, sem espelho d`água");

    @Getter
    private final String descricao;

    Habitat(String descricao) {
        this.descricao = descricao;
    }

    @Converter(autoApply = true)
    public static class Mapeador implements AttributeConverter<Habitat, String> {
        @Override
        public String convertToDatabaseColumn(Habitat descricao) {

            return descricao.getDescricao();
        }

        @Override
        public Habitat convertToEntityAttribute(String descricao) {
            switch (descricao) {
                case "Campo Seco Baixo":
                    return CSB;
                case "Campo alagado, Campo úmido, Várzeas alagadas":
                    return CAUVA;
                case "Banhado com Espelho d’água":
                    return BED;
                case "Campo seco alto campo sujo":
                    return CSACS;
                case "Campo com árvores, arbustos ou arvoretas":
                    return CCAAA;
                case "Banhado com vegetação alta, em geral palha ou gravatá, sem espelho d`água":
                    return BVA;
                default:
                    return null;
            }
        }
    }


    /**
     * Usado nos relatórios
     */
    public static final Set<Habitat> POSSIVEIS_HABITATS =
            new HashSet<>( Arrays.asList(
                    Habitat.CSB,
                    Habitat.CAUVA,
                    Habitat.BED,
                    Habitat.CSACS,
                    Habitat.CCAAA,
                    Habitat.BVA )
            );

}
