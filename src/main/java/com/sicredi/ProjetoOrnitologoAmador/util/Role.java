package com.sicredi.ProjetoOrnitologoAmador.util;

public enum Role {
    USER("USER");

    private String nome;
    Role(String nome){
        this.nome = nome;
    }

    public String getNome() {
        return this.nome;
    }
}

