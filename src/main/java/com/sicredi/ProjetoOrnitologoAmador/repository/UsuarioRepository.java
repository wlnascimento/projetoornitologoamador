package com.sicredi.ProjetoOrnitologoAmador.repository;

import com.sicredi.ProjetoOrnitologoAmador.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
    Usuario findUserById(Long id);

    Usuario findByUserID(String userID);

}
