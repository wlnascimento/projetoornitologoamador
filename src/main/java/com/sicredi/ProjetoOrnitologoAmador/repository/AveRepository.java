package com.sicredi.ProjetoOrnitologoAmador.repository;

import com.sicredi.ProjetoOrnitologoAmador.model.Ave;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AveRepository extends JpaRepository<Ave, Long> {

}
