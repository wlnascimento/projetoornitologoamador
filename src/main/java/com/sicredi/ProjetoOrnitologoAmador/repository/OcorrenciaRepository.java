package com.sicredi.ProjetoOrnitologoAmador.repository;

import com.sicredi.ProjetoOrnitologoAmador.model.Ocorrencia;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OcorrenciaRepository extends JpaRepository<Ocorrencia, Long> {
}
