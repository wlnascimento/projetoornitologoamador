package com.sicredi.ProjetoOrnitologoAmador.DTO;

import com.sicredi.ProjetoOrnitologoAmador.util.CorPredominante;
import com.sicredi.ProjetoOrnitologoAmador.util.Genero;
import com.sicredi.ProjetoOrnitologoAmador.util.Habitat;
import lombok.*;

import java.util.List;

@Data
@NoArgsConstructor
public class AveDTO {
    private String nome;
    private String nomeIngles;
    private String nomeLatim;
    private Integer tamanho;
    private Genero genero;
    private CorPredominante corPredominante;
    private String familia;
    private List<Habitat> habitats;
}
