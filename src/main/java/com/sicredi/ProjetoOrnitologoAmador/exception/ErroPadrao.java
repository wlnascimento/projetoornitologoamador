package com.sicredi.ProjetoOrnitologoAmador.exception;

import lombok.*;

import java.io.Serializable;
import java.time.Instant;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ErroPadrao implements Serializable {
    private Instant timestamp;
    private Integer status;
    private String error;
    private String trace;
    private String message;
    private String path;
}
