package com.sicredi.ProjetoOrnitologoAmador.exception;

public class EntityFoundException extends RuntimeException{

    public EntityFoundException(String msg) {
        super(msg);
    }
}
