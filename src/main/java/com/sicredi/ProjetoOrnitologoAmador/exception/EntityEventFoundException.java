package com.sicredi.ProjetoOrnitologoAmador.exception;

public class EntityEventFoundException extends RuntimeException{

    public EntityEventFoundException(String msg) {
        super(msg);
    }
}
