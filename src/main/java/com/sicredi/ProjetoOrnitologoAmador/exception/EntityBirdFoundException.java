package com.sicredi.ProjetoOrnitologoAmador.exception;

public class EntityBirdFoundException extends RuntimeException{

    public EntityBirdFoundException(String msg) {
        super(msg);
    }
}
