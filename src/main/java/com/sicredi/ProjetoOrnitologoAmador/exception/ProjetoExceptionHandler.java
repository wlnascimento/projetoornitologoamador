package com.sicredi.ProjetoOrnitologoAmador.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;
import java.util.Arrays;

@ControllerAdvice
public class ProjetoExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<ErroPadrao> entityNotFoundException(EntityNotFoundException exc, HttpServletRequest request){
        ErroPadrao err = new ErroPadrao();

        err.setTimestamp(Instant.now());
        err.setStatus(HttpStatus.NOT_FOUND.value());
        err.setError("Usuário não encontrado!");
        err.setMessage(exc.getMessage());
        err.setTrace(Arrays.toString(exc.getStackTrace()));
        err.setPath(request.getRequestURI());

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(err);
    }

    @ExceptionHandler(EntityFoundException.class)
    public ResponseEntity<ErroPadrao> entityFoundException(EntityFoundException exc, HttpServletRequest request){
        ErroPadrao err = new ErroPadrao();

        err.setTimestamp(Instant.now());
        err.setStatus(HttpStatus.FOUND.value());
        err.setError("ID do cadastro ou userID já cadastrados no banco de dados!");
        err.setMessage(exc.getMessage());
        err.setTrace(Arrays.toString(exc.getStackTrace()));
        err.setPath(request.getRequestURI());

        return ResponseEntity.status(HttpStatus.FOUND).body(err);
    }


    @ExceptionHandler(EntityListEventNotFoundException.class)
    public ResponseEntity<ErroPadrao> entityListEventNotFoundException(EntityListEventNotFoundException exc, HttpServletRequest request){
        ErroPadrao err = new ErroPadrao();

        err.setTimestamp(Instant.now());
        err.setStatus(HttpStatus.NO_CONTENT.value());
        err.setError("Não encontrados os dados solicitados. Lista de ocorrências está vazia.");
        err.setMessage(exc.getMessage());
        err.setTrace(Arrays.toString(exc.getStackTrace()));
        err.setPath(request.getRequestURI());

        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(err);
    }

    @ExceptionHandler(EntityEventFoundException.class)
    public ResponseEntity<ErroPadrao> entityEventFoundException(EntityEventFoundException exc, HttpServletRequest request){
        ErroPadrao err = new ErroPadrao();

        err.setTimestamp(Instant.now());
        err.setStatus(HttpStatus.FOUND.value());
        err.setError("ID informado na ocorrência já cadastrado no banco de dados!");
        err.setMessage(exc.getMessage());
        err.setTrace(Arrays.toString(exc.getStackTrace()));
        err.setPath(request.getRequestURI());

        return ResponseEntity.status(HttpStatus.FOUND).body(err);
    }

    @ExceptionHandler(EntityBirdFoundException.class)
    public ResponseEntity<ErroPadrao> entityBirdFoundException(EntityBirdFoundException exc, HttpServletRequest request){
        ErroPadrao err = new ErroPadrao();

        err.setTimestamp(Instant.now());
        err.setStatus(HttpStatus.FOUND.value());
        err.setError("ID informado no cadastro da ave já cadastrado no banco de dados!");
        err.setMessage(exc.getMessage());
        err.setTrace(Arrays.toString(exc.getStackTrace()));
        err.setPath(request.getRequestURI());

        return ResponseEntity.status(HttpStatus.FOUND).body(err);
    }
}
