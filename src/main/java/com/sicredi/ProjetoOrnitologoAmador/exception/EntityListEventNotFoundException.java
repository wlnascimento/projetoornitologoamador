package com.sicredi.ProjetoOrnitologoAmador.exception;

public class EntityListEventNotFoundException extends RuntimeException{

    public EntityListEventNotFoundException(String msg) {
        super(msg);
    }
}
