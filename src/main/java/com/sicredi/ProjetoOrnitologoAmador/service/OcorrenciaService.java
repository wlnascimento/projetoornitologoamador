package com.sicredi.ProjetoOrnitologoAmador.service;

import com.sicredi.ProjetoOrnitologoAmador.exception.EntityEventFoundException;
import com.sicredi.ProjetoOrnitologoAmador.exception.EntityNotFoundException;
import com.sicredi.ProjetoOrnitologoAmador.model.Ocorrencia;
import com.sicredi.ProjetoOrnitologoAmador.model.Usuario;
import com.sicredi.ProjetoOrnitologoAmador.repository.OcorrenciaRepository;
import com.sicredi.ProjetoOrnitologoAmador.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OcorrenciaService {

    @Autowired OcorrenciaRepository ocorrenciaRepository;
    @Autowired UsuarioRepository usuarioRepository;
    @Autowired AveService aveService;

    public ResponseEntity<Ocorrencia> cadastrarOcorrenciaUsuario(String nomeUsuario, Ocorrencia novaOcorrencia) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;

        if (principal instanceof UserDetails){
            username = ((UserDetails)principal).getUsername();
        }
        else{
            username = principal.toString();
        }

        if(username.equalsIgnoreCase(nomeUsuario)) {
            if (!usuarioRepository.findAll().isEmpty()) {
                for (Usuario usuario : usuarioRepository.findAll()) {
                    if (usuario.getUserID().equalsIgnoreCase(nomeUsuario)) {

                        if (novaOcorrencia.getId() != null) {

                            for (Ocorrencia ocorrencia : ocorrenciaRepository.findAll()) {
                                if (ocorrencia.getId() == novaOcorrencia.getId()) {
                                    throw new EntityEventFoundException("ID " + ocorrencia.getId() + " informado na ocorrência já existe no banco de dados!");
                                }
                            }
                        }

                        aveService.salvarAve(novaOcorrencia.getAve());
                        ocorrenciaRepository.save(novaOcorrencia);
                        usuario.getOcorrencias().add(novaOcorrencia);
                        usuarioRepository.save(usuario);

                        return ResponseEntity.status(HttpStatus.CREATED).body(novaOcorrencia);
                    }
                }
            }

            throw new EntityNotFoundException("Usuário " + nomeUsuario + " não encontrado!");
        }

        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

}
