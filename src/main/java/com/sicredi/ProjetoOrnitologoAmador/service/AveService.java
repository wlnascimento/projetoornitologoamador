package com.sicredi.ProjetoOrnitologoAmador.service;

import com.sicredi.ProjetoOrnitologoAmador.exception.EntityBirdFoundException;
import com.sicredi.ProjetoOrnitologoAmador.model.Ave;
import com.sicredi.ProjetoOrnitologoAmador.repository.AveRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor

@Service
public class AveService {

    @Autowired AveRepository aveRepository;

    public List<Ave> listAll() {
        return aveRepository.findAll();
    }

    public ResponseEntity<List<Ave>> buscarAvePorNome(String nome) {
        List<Ave> ListaAves = new ArrayList<>();

        if(!aveRepository.findAll().isEmpty()) {

            aveRepository.findAll().forEach(ave -> {
                if ((ave.getNome().equalsIgnoreCase(nome)) || (ave.getNome().toLowerCase().contains(nome.toLowerCase()))) {
                    ListaAves.add(ave);
                }
            });

            if(ListaAves.isEmpty()){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }
            else {
                return ResponseEntity.status(HttpStatus.FOUND).body(ListaAves);
            }
        }
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    public ResponseEntity<Ave> salvarAve(Ave novaAve) {

        // VALIDA SE A TABELA DE AVES ESTA VAZIA OU NÃO. SE ESTIVER VAZIA SALVA A AVE E RETORNA, CASO CONTRARIO ENTRA NO FOREACH
        if(!aveRepository.findAll().isEmpty()) {

            if(novaAve.getId() != null) {
                for (Ave ave : aveRepository.findAll()) {

                    if(ave.getId().equals(novaAve.getId())){
                        throw new EntityBirdFoundException("ID " + novaAve.getId() + " informado no cadastro da ave já existe no banco de dados!");
                    }
                    else if ((!ave.getId().equals(novaAve.getId()))
                            && ((ave.getNome().equalsIgnoreCase(novaAve.getNome()))
                            && (ave.getFamilia().equalsIgnoreCase(novaAve.getFamilia()))
                            && (ave.getGenero() == novaAve.getGenero())
                            && (ave.getCorPredominante() == novaAve.getCorPredominante())
                            && (ave.getTamanho().equals(novaAve.getTamanho())))
                    ) {
                        throw new EntityBirdFoundException("A ave " + novaAve.getNome() + " já cadastrada no banco de dados!");
                    }
                }
            }
            else{
                for (Ave ave : aveRepository.findAll()) {
                    if((ave.getNome().equalsIgnoreCase(novaAve.getNome())) && (ave.getFamilia().equalsIgnoreCase(novaAve.getFamilia()))
                            && (ave.getGenero() == novaAve.getGenero()) && (ave.getCorPredominante() == novaAve.getCorPredominante())
                            && (ave.getTamanho().equals(novaAve.getTamanho())))
                    {
                        throw new EntityBirdFoundException("A ave " + novaAve.getNome() + " já está cadastrada no banco de dados!");
                    }
                }
            }
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(aveRepository.save(novaAve));
    }
}

