package com.sicredi.ProjetoOrnitologoAmador.service;

import com.sicredi.ProjetoOrnitologoAmador.exception.EntityFoundException;
import com.sicredi.ProjetoOrnitologoAmador.exception.EntityListEventNotFoundException;
import com.sicredi.ProjetoOrnitologoAmador.exception.EntityNotFoundException;
import com.sicredi.ProjetoOrnitologoAmador.model.Ocorrencia;
import com.sicredi.ProjetoOrnitologoAmador.model.Usuario;
import com.sicredi.ProjetoOrnitologoAmador.repository.UsuarioRepository;
import com.sicredi.ProjetoOrnitologoAmador.util.Role;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@AllArgsConstructor
public class UsuarioService {

    @Autowired private UsuarioRepository usuarioRepository;
    @Autowired private PasswordEncoder passwordEncoder;

    public ResponseEntity<Usuario> salvarUsuario(Usuario novoUsuario) {
        Usuario userIDEncontrado = validarUsuarioExistente(novoUsuario);
        Usuario user = new Usuario(novoUsuario.getNome(), novoUsuario.getUserID(), passwordEncoder.encode(novoUsuario.getSenha()));
        user.setRole(Role.USER.getNome());

        if(novoUsuario.getId() != null) {
            if (usuarioRepository.findById(novoUsuario.getId()).isPresent()) {
                throw new EntityFoundException("ID " + novoUsuario.getId() + " já cadastrado.");
            }
            else if((userIDEncontrado != null)){
                throw new EntityFoundException("userID " + novoUsuario.getUserID() + " já cadastrado.");
            }
        }
        else if((userIDEncontrado != null)){
            throw new EntityFoundException("userID " + novoUsuario.getUserID() + " já cadastrado.");
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(usuarioRepository.save(user));
    }

    public ResponseEntity<Usuario> consultarUsuario(String nomeUsuario) {
        if(!usuarioRepository.findAll().isEmpty()) {
            for (Usuario usuario : usuarioRepository.findAll()) {
                if (usuario.getUserID().equalsIgnoreCase(nomeUsuario)) {
                    return ResponseEntity.status(HttpStatus.FOUND).body(usuario);
                }
            }
        }
        throw new EntityNotFoundException("Usuário " + nomeUsuario + " não encontrado!");
    }

    public ResponseEntity<List<Ocorrencia>> listarOcorrenciaDoUsuario(String nomeUsuario) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;

        if (principal instanceof UserDetails){
            username = ((UserDetails)principal).getUsername();
        }
        else{
            username = principal.toString();
        }

        if(username.equals(nomeUsuario)) {
            if (!usuarioRepository.findAll().isEmpty()) {
                for (Usuario usuario : usuarioRepository.findAll()) {

                    if (usuario.getUserID().equalsIgnoreCase(nomeUsuario)) {
                        if (usuario.getOcorrencias().isEmpty()) {
                            throw new EntityListEventNotFoundException("A lista de ocorrências do usuário " + nomeUsuario + " não encontrada ou está vazia.");
                        } else {
                            return ResponseEntity.status(HttpStatus.FOUND).body(usuario.getOcorrencias());
                        }
                    }
                }
            } else {
                throw new EntityListEventNotFoundException("A lista de ocorrências do usuário " + nomeUsuario + " não encontrada ou está vazia.");
            }

            throw new EntityNotFoundException("Usuário " + nomeUsuario + " não encontrado!");
        }

        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    public Usuario validarUsuarioExistente(Usuario usuario){

        for (Usuario user1 : usuarioRepository.findAll()) {
            if(user1.getUserID().equalsIgnoreCase(usuario.getUserID())){
                return user1;
            }
        }
        return null;
    }

    public ResponseEntity<Boolean> login(Usuario usuario) {
        Usuario user = usuarioRepository.findByUserID(usuario.getUserID());
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        if(passwordEncoder.matches(usuario.getSenha(), user.getSenha())){
            return ResponseEntity.status(HttpStatus.OK).body(true);
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(false);
    }
}
